from django.shortcuts import get_object_or_404, redirect, render
from django.http import HttpResponse
from django.contrib.auth import logout, authenticate, login
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone
from django.template import loader
from django.contrib.auth.forms import AuthenticationForm
from .models import Contenido, Comentario, LoginForm
import random


def style(request):
    colors = ["blue", "gold", "red", "aqua", "green", "black", "yellow"]
    selected_text_color = random.choice(colors)
    selected_background_color = random.choice(colors)

    response = f"""
    body {{
        margin: 10px 20% 50px 70px;
        font-family: sans-serif;
        color: {selected_text_color};
        background: {selected_background_color};
    }}
    """

    return HttpResponse(response, content_type="text/css")

@csrf_exempt
def get_content(request, llave):
    if request.method == "POST":
        valor = request.POST.get('valor', '')
        try:
            content = Contenido.objects.get(clave=llave)
            content.delete()
        except Contenido.DoesNotExist:
            pass
        content = Contenido(clave=llave, valor=valor)
        content.save()
        return HttpResponse("Contenido agregado correctamente")
    else:
        try:
            content = Contenido.objects.get(clave=llave)
            response = "El valor de la llave es: " + content.valor
            return HttpResponse(response)
        except Contenido.DoesNotExist:
            if request.user.is_authenticated:
                return HttpResponse("No se encontró contenido para esta clave", status=404)
            else:
                return HttpResponse("No estás autenticado. <a href='login'>Iniciar sesión</a>", status=401)


def index(request):
    template = loader.get_template('index.html')
    content_list = Contenido.objects.all()
    context = {'content_list': content_list}
    return HttpResponse(template.render(context, request))

def loggedIn(request):
    if request.user.is_authenticated:
        answer = "Logged in as " + request.user.username
    else:
        answer = "Not logged in. <a href='/admin/'>Log in</a>"
    return HttpResponse(answer)

def loggedOut(request):
    logout(request)
    return HttpResponse("logged out")


def mylogin(request):
    if request.method == 'POST':
        form = AuthenticationForm(request, request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('/')
    else:
        form = LoginForm()

    template = loader.get_template('registration/login.html')
    rendered_template = template.render({'form': form}, request)
    return HttpResponse(rendered_template)


def add_comment(request, contenido_id):
    contenido = get_object_or_404(Contenido, pk=contenido_id)
    if request.method == 'POST':
        form = Comentario(request.POST)
        if form.is_valid():
            comentario = form.save(commit=False)
            comentario.contenido = contenido
            comentario.autor = request.user
            comentario.save()
            return HttpResponse("Comentario agregado correctamente")
    else:
        form = Comentario()
    return render(request, 'content.html', {'form': form})

