from django.db import models
from django import forms

# Create your models here.
class Contenido(models.Model):
    clave=models.CharField(max_length=100)
    valor=models.TextField()

    def __str__(self):
        return self.clave


class Comentario(models.Model):
    content = models.ForeignKey(Contenido, on_delete=models.CASCADE)
    titulo = models.CharField(max_length=200)
    cuerpo = models.TextField(blank=False)
    fecha = models.DateTimeField('publicado')


class LoginForm(forms.Form):
    username = forms.CharField(label='Username')
    password = forms.CharField(label='Password', widget=forms.PasswordInput)