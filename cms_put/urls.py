from django.urls import path
from . import views
from django.contrib.auth.views import LoginView

urlpatterns = [
    path("", views.index, name="index"),
    path('main.css', views.style),
    path("loggedin", views.loggedIn),
    path("login", views.mylogin),
    path("loggedout", views.loggedOut),
    path("<llave>", views.get_content),
]