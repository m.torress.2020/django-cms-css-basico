# Generated by Django 5.0.3 on 2024-04-22 15:09

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("cms_put", "0001_initial"),
    ]

    operations = [
        migrations.CreateModel(
            name="Contenido",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("key", models.CharField(max_length=100)),
                ("value", models.TextField()),
            ],
        ),
        migrations.AlterField(
            model_name="comentario",
            name="content",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE, to="cms_put.contenido"
            ),
        ),
        migrations.DeleteModel(
            name="Content",
        ),
    ]
